<h2>Game 24</h2>
<table>
    <tr>
        <td><label>number1 :</label></td>
        <td><input type="number" name="number1" id="number1"></td>
    </tr>
    <tr>
        <td><label>number2 :</label></td>
        <td><input type="number" name="number2" id="number2"></td>
    </tr>
    <tr>
        <td><label>number3 :</label></td>
        <td><input type="number" name="number3" id="number3"></td>
    </tr>
    <tr>
        <td><label>number5 :</label></td>
        <td><input type="number" name="number4" id="number4"></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
            <button type="button"
                    onclick="startGame()">
                Submit
            </button>
        </td>
    </tr>
</table>
<p>
<div>
    <h2>Result</h2>
    <textarea rows=11 cols=40 name="display" id="display"></textarea>
</div>
</p>
<script src="https://code.jquery.com/jquery-git.min.js"></script>
<script type="text/javascript">
    function startGame() {
        var number1 = $('#number1').val();
        var number2 = $('#number2').val();
        var number3 = $('#number3').val();
        var number4 = $('#number4').val();
        var display = $('#display').val();

        do24(number1, number2, number3, number4, display);
    }

    function do24(a, b, c, d, solution) {
        $nums = ["", a, b, c, d]
        ops = ["", "+", "-", "*", "/"]
        solution = "working...\n"
        maxsol = 1
        sol = 0

        for (i = 1; i < 5 && sol < maxsol; i++) {
            for (j = 1; j < 5 && sol < maxsol; j++) {
                if (j == i)
                    continue
                for (k = 1; k < 5 && sol < maxsol; k++) {
                    if (k == i | k == j
                    ) continue
                    var l = 1 + 2 + 3 + 4 - i - j - k

                    for (p = 1; p < 5 && sol < maxsol; p++) {
                        for (q = 1; q < 5 && sol < maxsol; q++) {
                            for (r = 1; r < 5 && sol < maxsol; r++) {

                                for (p1o = 1; p1o < 4 && sol < maxsol; p1o++) {
                                    for (p1c = p1o + 1; p1c < 5 && sol < maxsol; p1c++) {
                                        for (ispecial = 1; ispecial <= 2; ispecial++) {

                                            if (ispecial == 1 && p1o == 1 && p1c == 2) {
                                                p2o = 3;
                                                p2c = 4
                                            } else {
                                                p2o = 0;
                                                p2c = 0
                                            }
                                            expr = p1o == 1 ? '(' : ''
                                            expr += $nums[i] + ops[p] + (p1o == 2 ? '(' : '')
                                            expr += $nums[j] + (p1c == 2 ? ')' : '')
                                            expr += ops[q] + (p1o == 3 || p2o == 3 ? '(' : '')
                                            expr += $nums[k] + (p1c == 3 ? ')' : '')
                                            expr += ops[r] + $nums[l] + (p1c == 4 || p2c == 4 ? ')' : '')
                                            ev = eval(expr)
                                            if (ev > 23.9999 && ev < 24.00001) {
                                                if (sol == 0) solution = '';
                                                expr = expr + ' = ' + Math.round(ev);
                                                if (solution.indexOf(expr) < 0) {
                                                    solution += expr + "\n";
                                                    sol++
                                                }
                                            } // end of ev=24
                                        } // next ispecial
                                    } // next p1c
                                } // next p1o
                            } // next  r
                        } // next  q
                    } // next  p
                } //next k
            } // next j
        } // next i
        if (sol == 0)
            solution = "Game Over"

        $('#display').html(solution);
    }
</script>